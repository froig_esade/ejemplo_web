//
//  ViewController.swift
//  Exemple Navegador Web
//
//  Created by Francesc Roig on 7/4/18.
//  Copyright © 2018 Francesc Roig. All rights reserved.
//

import UIKit
import WebKit


// IMPORTANTE !!
// Es necesario añadir las directivas de privacidad:
//   "App Transport Security Settings"
//     "Allow Arbitrary Loads -> YES" (YES está definido en la columna de 'Value')
// en Info.plist. De lo contrario no se podran cargar contenidos web externos.




class ViewController: UIViewController {

    
    @IBOutlet weak var navegador: WKWebView!
    
    @IBOutlet weak var textURL: UITextField!
    
    
    /*
     * La función 'viewDidLoad' se invoca automàticamente en el momento de
     * iniciar la apliccaión.
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Se define la 'url' que se cargará cuando se inicie la aplicación.
        let url = URL(string: "http://www.esade.edu")
        
        // Se define la 'request' a partir de la URL para poder cargarla en
        // en el navegador.
        let peticion = URLRequest(url: url!)
        
        // Se carga la request en el navegador para que se cargue su contenido.
        navegador.load(peticion)
    }
    
    
    
    /*
     * Acción: 'Did end on exit'
     * Permite que una vez se pulse el botón de 'Intro' del teclado
     * virtual, se cargue la url introducida en el campo de texto.
     * IMPORTANTE: Las url deben empezar por 'http:// .....', de lo
     * contrario no se cargaran.
     */
    @IBAction func accionTextURLEnter(_ sender: Any) {
        accionGo(sender)
    }
    
    
    
    /*
     * Acción correspondiente al botón de 'go'.
     */
    @IBAction func accionGo(_ sender: Any) {
        
        // Se comprueba que el texto escrito por el usuario en el 'textURL' se puede
        // convertir en una URL que pueda ser cargada.
        if let url = URL(string: textURL.text!) {
            // La URL es correcta, por lo tanto se define la petición 'request'
            // a partir de la URL para poder cargarla en en el navegador.
            let peticion = URLRequest(url: url)
            // Se carga la 'request' en el navegador.
            navegador.load(peticion)
        } else {
            // En el caso que la URL escrita por el usuario no sea correcta,
            // se ejecuta esta parte del condicional y se muestra este aviso.
            print("Error en la dirección web.")
        }
    }
    
    
    
    /*
     * Acción correspondiente al botón de 'back'.
     */
    @IBAction func accionBack(_ sender: Any) {
        // Invocamos la funcion 'goBack()' del componente navegador.
        navegador.goBack()
    }

    
    
    /*
     * Acción correspondiente al botón de 'Fordware'.
     */
    @IBAction func accionFordward(_ sender: Any) {
        // Invocamos la funcion 'goForward()' del componente navegador.
        navegador.goForward()
    }
    

} // Final del bloque pricipal.

